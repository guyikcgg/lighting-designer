
const assetList = [
    {
        name: "Subjects",
        id: "subjects",
        allowDeform: false,
        allowResize: true,
        assets: [
            {
                name: "Generic subject",
                id: "subject-1",
                icon: "subject-1.svg",
                properties: [
                    {type: "color", n: 0, mode: "normal", id: "Feet"},
                    {type: "color", n: 1, mode: "normal", id: "Clothes"},
                    {type: "color", n: 2, mode: "normal", id: "Skin"},
                    {type: "color", n: 3, mode: "normal", id: "Hair"},
                ]
            },
            {
                name: "Walking pedestrian (by enolynn, openclipart.org)",
                id: "subject-pedestrian",
                icon: "subject-pedestrians-granny.svg",
                defaultScale: 2.2,
                properties: [
                    {type: "select", id: "Models", options: [
                        {
                            name: "Kid",
                            icon: "subject-pedestrians-kid.svg",
                        },
                        {
                            name: "Kid playing football",
                            icon: "subject-pedestrians-kid-ball.svg",
                        },
                        {
                            name: "Granny",
                            icon: "subject-pedestrians-granny.svg",
                        },
                        {
                            name: "Elder with cane",
                            icon: "subject-pedestrians-elder-cane.svg",
                        },
                        {
                            name: "Man",
                            icon: "subject-pedestrians-man.svg",
                        },
                        {
                            name: "Woman",
                            icon: "subject-pedestrians-woman.svg",
                        },
                        {
                            name: "Woman with pushchair",
                            icon: "subject-pedestrians-pushchair.svg",
                        },
                    ]}
                ]
            },
            {
                name: "Two-wheeler (by enolynn, openclipart.org)",
                id: "subject-two-wheeler",
                icon: "subject-two-wheeler-scooter.svg",
                defaultScale: 2.2,
                properties: [
                    {type: "select", id: "Models", options: [
                        {
                            name: "Bike",
                            icon: "subject-two-wheeler-bike.svg",
                        },
                        {
                            name: "Scooter",
                            icon: "subject-two-wheeler-scooter.svg",
                        },
                        {
                            name: "Motorcycle",
                            icon: "subject-two-wheeler-motorcycle.svg",
                        },
                    ]}
                ]
            },
        ]
    },
    {
        name: "Cameras",
        id: "cameras",
        allowDeform: false,
        allowResize: true,
        assets: [
            {
                name: "DSLR Camera",
                id: "dslr",
                icon: "dslr.svg",
                defaultScale: 0.7
            },
        ]
    },
    {
        name: "Light sources",
        id: "light-sources",
        allowDeform: false,
        allowResize: true,
        assets: [
            {
                name: "Flash",
                id: "flash",
                icon: "flash-parabola.svg",
                properties: [
                    {type: "select", id: "Models", options: [
                        {
                            name: "Flash with parabola",
                            icon: "flash-parabola.svg",
                        },
                        {
                            name: "Flash with softbox",
                            icon: "flash-softbox.svg",
                        },
                        {
                            name: "Flash (without accessories)",
                            icon: "flash-alone.svg",
                        },
                    ]}
                ]
            },
            {
                name: "Fresnel",
                id: "fresnel",
                icon: "fresnel.svg",
                defaultScale: 0.7
            },
            {
                name: "Hallogen",
                id: "hallogen",
                icon: "hallogen.svg",
                defaultScale: 0.7
            },
            {
                name: "Panel fluo",
                id: "panel-fluo",
                icon: "panel-fluo.svg",
                defaultScale: 0.7
            },
            {
                name: "Tube",
                id: "tube",
                icon: "tube.svg",
            },
            {
                name: "Sun",
                id: "sun",
                icon: "sun.svg",
                defaultScale: 0.6
            },
        ]
    },
    {
        name: "Accessories",
        id: "accessories",
        allowResize: true,
        assets: [
            {
                name: "Light box",
                id: "light-box",
                icon: "light-box.svg",
            },
            {
                name: "Panel",
                id: "panel",
                icon: "panel-black.svg",
                allowDeform: true,
                properties: [
                    {type: "select", id: "Models", options: [
                        {
                            name: "White panel",
                            icon: "panel-white.svg",
                        },
                        {
                            name: "Black panel",
                            icon: "panel-black.svg",
                        },
                        {
                            name: "Diffusor panel",
                            icon: "panel-diffusor.svg",
                        },
                    ]},
                ]
            },
            {
                name: "Gel filter",
                id: "gel-filter",
                icon: "gel-filter.svg",
                allowDeform: true,
                properties: [
                    {type: "color", n: 0, mode: "normal", id: "Filter color"}
                ]
            },
            {
                name: "Softbox",
                id: "softbox",
                icon: "softbox.svg",
                allowDeform: true,
            },
            {
                name: "Light meter",
                id: "light-meter",
                icon: "light-meter.svg"
            },
            {
                name: "Handheld reflector/difusor",
                id: "reflector",
                icon: "reflector-silver.svg",
                allowDeform: true,
                properties: [
                    {type: "select", id: "Models", options: [
                        {
                            name: "Silver reflector",
                            icon: "reflector-silver.svg",
                        },
                        {
                            name: "White reflector",
                            icon: "reflector-white.svg",
                        },
                        {
                            name: "Black reflector",
                            icon: "reflector-black.svg",
                        },
                        {
                            name: "Golden reflector",
                            icon: "reflector-golden.svg",
                        },
                    ]},
                ]
            },
            {
                name: "Umbrella",
                id: "umbrella",
                icon: "umbrella-black.svg",
                properties: [
                    {type: "select", id: "Models", options: [
                        {
                            name: "White umbrella",
                            icon: "umbrella-white.svg",
                        },
                        {
                            name: "Black umbrella",
                            icon: "umbrella-black.svg",
                        }
                    ]},
                ]
            },
            {
                name: "Light stand",
                id: "light-stand",
                icon: "light-stand.svg",
            },
            {
                name: "Backdrop",
                id: "backdrop",
                icon: "backdrop-long.svg",
                properties: [
                    {type: "color", n: 7, mode: "multiply", id: "Paper color"},
                    {type: "select", id: "Models", options: [
                        {
                            name: "Long backdrop",
                            icon: "backdrop-long.svg",
                        },
                        {
                            name: "Vertical backdrop",
                            icon: "backdrop-vertical.svg",
                        }
                    ]},
                ]
            },
        ]
    },
    {
        name: "Objects and architecture",
        allowResize: true,
        assets: [
            {
                name: "Plant",
                id: "plant",
                icon: "plant.svg",
                properties: [
                    {type: "select", id: "Models", options: [
                        {
                            name: "Plant (without flower)",
                            icon: "plant.svg",
                        },
                        {
                            name: "Plant with flower",
                            icon: "plant-flower.svg",
                        }
                    ]},
                ]
            },
            {
                name: "Laptop",
                id: "laptop",
                icon: "laptop.svg",
            },
            {
                name: "Computer display",
                id: "computer-display",
                icon: "computer-display.svg",
            },
            {
                name: "Office chair",
                id: "chair-office",
                icon: "chair-office.svg",
            },
            {
                name: "Door",
                id: "door",
                icon: "door-90deg.svg",
                properties: [
                    {type: "select", id: "Models", options: [
                        {
                            name: "Door 60 deg",
                            icon: "door-60deg.svg",
                        },
                        {
                            name: "Door 90 deg",
                            icon: "door-90deg.svg",
                        },
                        {
                            name: "Door 105 deg",
                            icon: "door-105deg.svg",
                        },
                    ]},
                ]
            },
            {
                name: "Tree 1 (by enolynn, openclipart.org)",
                id: "tree-1",
                icon: "tree-1-1.svg",
                defaultScale: 1.5,
                properties: [
                    {type: "select", id: "Models", options: [
                        {
                            name: "Tree 1 (option 1)",
                            icon: "tree-1-1.svg",
                        },
                        {
                            name: "Tree 1 (option 2)",
                            icon: "tree-1-2.svg",
                        },
                        {
                            name: "Tree 1 (option 3)",
                            icon: "tree-1-3.svg",
                        },
                        {
                            name: "Tree 1 (option 4)",
                            icon: "tree-1-4.svg",
                        },
                    ]},
                ]
            },
            {
                name: "Tree 2 (by enolynn, openclipart.org)",
                id: "tree-2",
                icon: "tree-2-1.svg",
                defaultScale: 1.5,
                properties: [
                    {type: "select", id: "Models", options: [
                        {
                            name: "Tree 2 (option 1)",
                            icon: "tree-2-1.svg",
                        },
                        {
                            name: "Tree 2 (option 2)",
                            icon: "tree-2-2.svg",
                        },
                        {
                            name: "Tree 2 (option 3)",
                            icon: "tree-2-3.svg",
                        },
                        {
                            name: "Tree 2 (option 4)",
                            icon: "tree-2-4.svg",
                        },
                        {
                            name: "Tree 2 (option 5)",
                            icon: "tree-2-5.svg",
                        },
                    ]},
                ]
            },
            {
                name: "Roof (by enolynn, openclipart.org)",
                id: "roof",
                icon: "roof-2.svg",
                defaultScale: 2.5,
                properties: [
                    {type: "select", id: "Models", options: [
                        {
                            name: "Roof (option 1)",
                            icon: "roof-1.svg",
                        },
                        {
                            name: "Roof (option 2)",
                            icon: "roof-2.svg",
                        },
                        {
                            name: "Roof (option 3)",
                            icon: "roof-3.svg",
                        },
                    ]},
                ]
            },
            {
                name: "Bench (by enolynn, openclipart.org)",
                id: "bench",
                icon: "bench.svg",
                defaultScale: 2.5,
                allowDeform: true,
                properties: [
                    {type: "color", n: 12, mode: "multiply", id: "Color"}
                ]
            },
            {
                name: "Ball",
                id: "ball",
                icon: "ball.svg",
                defaultScale: 1,
                allowDeform: false,
                properties: [
                    {type: "color", n: 1, mode: "color-burn", id: "Color"}
                ]
            },
        ]
    }
]
