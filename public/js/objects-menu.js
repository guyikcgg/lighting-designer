

// Properties for each asset
assetList.forEach(function(category) {
    // Add the category to the left menu
    $('#objects').append(
        '<li class="accordion-item" data-accordion-item>'
         + '<a href="#" class="accordion-title">' + category.name + '</a>'
         + '<div class="accordion-content" data-tab-content>'
           + '<div class="hollow button-group button-cell"></div>'
         + '</div>'
      + '</li>'
    );

    category.assets.forEach(function (asset) {
        // Here we keep all the info regarding the asset. Some properties default to the type/group
        asset.category = category.id;
        asset.allowResize ??= (category.allowResize ?? false);
        asset.allowDeform ??= (category.allowDeform ?? false);

        // Add button corresponding to this asset on the left panel
        $('#objects .button-group:last').append(
            '<button class="button" data-tooltip title="' + asset.name + '">'
                + '<img src="assets/' + asset.icon + '" alt="' + asset.name + '"/>'
          + '</button>'
        );
        $('#objects .button-group:last .button:last').click(function() {
            addObjectFromAsset(asset);
        });

        // Create a section in the right panel for this asset
        asset.panel = $('<div id="panel-asset-' + asset.id + '" class="panel right-panel hide"></div>');
        asset.panel.append('<h6>Options for ' + asset.name + '</h6>');
        $('#panels').append(asset.panel);


        // Add an input for the asset's id
        asset.nameInput = $('<input type="text" class="borders-on-active" title="Asset\'s name">');
        asset.panel.append(asset.nameInput);

        asset.nameInput.focus(function() {
            this.activeObject = canvas.getActiveObject();
            this.select();
        });
        asset.nameInput.focusout(function(e) {
            // FIXME If a new object is selected, the id of the newly selected object will be taken,
            // because this.value is updated before the events trigger (see app.js)
            // Also, the id should be saved when pressing enter or Ctrl+S
            this.activeObject.objProperties.name = this.value;
            saveDiagramStateToUndoStack();
        });

        // Render asset's properties on right panel and allow changing them
        if (asset.properties) {
            // Add an input for each asset's property
            asset.properties.forEach(function(p) {
                switch (p.type) {
                case "color":
                    // Create a container for the color buttons
                    if (!asset.panelColor) {
                        asset.panelColor = $('<div class="hollow button-group button-cell"></div>');
                        asset.panel.append(asset.panelColor);   
                    }
                    p.control = $('<input class="button" type="color" title="' + p.id + '">');
                    asset.panelColor.append(p.control);
                    p.control.change(function(e) {
                        // Change the color of the nth object (as indicated by the property)
                        canvas.getActiveObject().getObjects()[p.n].set('fill', this.value);
                        canvas.getActiveObject().setCoords();
                        canvas.renderAll();
                        saveDiagramStateToUndoStack();
                    });
                    break;
                case "select":
                    asset.model = asset.icon;
                    p.control = $('<select title="' + p.id + '"></select>');
                    asset.panel.append(p.control);

                    p.options.forEach(function(opt) {
                        p.control.append(
                            '<option value="' + opt.icon + '">' + opt.name + '</option>'
                        );
                    });
                    p.control.change(function(e) {
                        // Delete the object and get a new one
                        replaceActiveObject(asset, this.value);
                    });
                    break;
                }
            });

        }
    });
});

function replaceActiveObject(asset, icon) {
    var activeObject = canvas.getActiveObject();
    var newAsset = Object.create(asset);

    var positionAndScale = {
        left: activeObject.left,
        top: activeObject.top,
        angle: activeObject.angle,
        scaleX: activeObject.scaleX,
        scaleY: activeObject.scaleY,
        flipX: activeObject.flipX,
        flipY: activeObject.flipY,
        originX: activeObject.originX,
        originY: activeObject.originY,
        shadow: activeObject.shadow,
    };

    // Load the corresponding icon
    newAsset.icon = icon;
    // Save the model into objProperties
    newAsset.model = icon;
    // Save the name into objProperties
    newAsset.name = activeObject.objProperties.name;

    // Handle properties affecting visualization (color)
    newAsset.color = [];
    asset.properties.forEach(function(p) {
        if (p.type === "color") {
            newAsset.color[p.n] = activeObject.getObjects()[p.n].fill;
        }
    });

    // Preserve z-index
    newAsset.zIndex = canvas.getObjects().indexOf(canvas.getActiveObject());
    
    canvas.remove(activeObject);
    canvas.discardActiveObject();

    addObjectFromAsset(newAsset, positionAndScale);
}

// Render the first icon. All the asset alternatives have the same properties!
function addObjectFromAsset(asset, positionAndScale) {
    var defaultLeft = 50;
    var defaultTop = 50;

    fabric.loadSVGFromURL('assets/' + asset.icon, function (objects, options) {
        // Handle properties affecting visualization (color)
        if (asset.properties) {
            asset.properties.forEach(function(p) {
                if (p.type === "color") {
                    // Set the blend mode from the asset class
                    objects[p.n].set({
                        globalCompositeOperation: p.mode
                    });
                    // Set the color (only if the asset was replaced)
                    if (asset.color && asset.color.length) {
                        objects[p.n].set({
                            fill: asset.color[p.n]
                        });   
                    }
                }
            });
        }
        var obj = fabric.util.groupSVGElements(objects, options);
        obj.setControlsVisibility({
            mt: asset.allowDeform,  // middle top 
            mb: asset.allowDeform,  // midle bottom
            ml: asset.allowDeform,  // middle left
            mr: asset.allowDeform,  // middle right
            tl: asset.allowResize,  // top left
            tr: asset.allowResize,  // top right
            bl: asset.allowResize,  // bottom left
            br: asset.allowResize   // bottom right
        });

        obj.assetClassProperties = Object.create(asset);
        obj.objProperties = {
            id: asset.id,
            name: asset.name,
            model: asset.model,
        };

        canvas.add(obj.scale(asset.defaultScale ?? 1));
        obj.set(positionAndScale ?? { left: defaultLeft, top: defaultTop }).setCoords();

        // Preserve z-index when changing asset's model
        if (asset.zIndex != undefined) {
            canvas._objects.splice(asset.zIndex, 0, canvas._objects.pop());
        }
        
        canvas.setActiveObject(obj);
        canvas.renderAll();

        // Set coords when moving, scaling or rotating the asset (included for legacy reasons)
        var setCoords = obj.setCoords.bind(obj);
        obj.on({
            moving: setCoords,
            scaling: setCoords,
            rotating: setCoords
        });
        saveDiagramStateToUndoStack();
    });

}

// Add a class to the object (normally loaded from JSON), so that it has complete function if it's an asset
// This function could be also named objectToAsset (or be an asset constructor on OOP)
function addAssetClassProperties(asset) {
    // Let's create a single list with the objects from all categories
    const assets = assetList.flatMap(assetList => assetList.assets);

    if (asset.type == 'activeSelection') {
        // This is not an asset, but multiple ones!
        asset.getObjects().forEach(function(a) {
            addAssetClassProperties(a);
        });
        return;
    }

    if(!asset.objProperties) {
        // Nothing to do... this is not a real asset
        return;
    }
    
    // Get the properties corresponding to each object, according to its id
    let assetClassProperties = assets.filter(a => a.id === asset.objProperties.id);

    // Add the class properties only to true assets
    if (assetClassProperties && assetClassProperties.length) {
        asset.assetClassProperties = assetClassProperties[0];

        // Load asset class' controls visibility
        asset.setControlsVisibility({
            mt: assetClassProperties[0].allowDeform,  // middle top 
            mb: assetClassProperties[0].allowDeform,  // midle bottom
            ml: assetClassProperties[0].allowDeform,  // middle left
            mr: assetClassProperties[0].allowDeform,  // middle right
            tl: assetClassProperties[0].allowResize,  // top left
            tr: assetClassProperties[0].allowResize,  // top right
            bl: assetClassProperties[0].allowResize,  // bottom left
            br: assetClassProperties[0].allowResize   // bottom right
        });
    }
}
