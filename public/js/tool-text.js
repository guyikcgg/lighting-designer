// Helper functions -- original source: http://fabricjs.com/js/itext.js and https://stackoverflow.com/questions/29934252/how-to-edit-a-fabricjs-itext-and-apply-new-styles-e-g-highlighting-etc
function setStyle(object, styleName, value) {
    var style = {};

    // If the text is not being edited, apply the styles to the whole selection
    if (!(object.setSelectionStyles && object.isEditing)) {
        object.selectAll();
    }

    style[styleName] = value;
    object.setSelectionStyles(style);
}

function getStyle(object, styleName) {
    var styles = [];

    // If the text is not being edited, return the styles for the first character
    if (!(object.setSelectionStyles && object.isEditing)) {
        styles = object.getSelectionStyles(0, 1);
    } else if (object.selectionEnd > object.selectionStart) {
        styles = object.getSelectionStyles();
    } else {
        styles = object.getSelectionStyles(
            object.selectionStart,
            object.selectionStart + 1);
    }

    // We'll check just the first character in the selection (position = 0)
    return styles[0][styleName];
}

function addHandler(id, fn, eventName) {
    document.getElementById(id)[eventName || 'onclick'] = function () {
        var el = this;
        if (obj = canvas.getActiveObject()) {
            let selectionEnd = obj.selectionEnd;
            fn.call(el, obj);
            obj.exitEditing().enterEditing();
            obj.selectionEnd = selectionEnd;
            canvas.renderAll();
            saveDiagramStateToUndoStack();
        }
    };
}


// Allow changing some styles
addHandler('text-bold', function (obj) {
    var isBold = getStyle(obj, 'fontWeight') === 'bold';
    setStyle(obj, 'fontWeight', isBold ? 'normal' : 'bold');
    obj.dirty = true;
});

addHandler('text-italic', function (obj) {
    var isItalic = getStyle(obj, 'fontWeight') === 'italic';
    setStyle(obj, 'fontStyle', isItalic ? 'normal' : 'italic');
    obj.dirty = true;
});

addHandler('text-underline', function (obj) {
    var isUnderlined = getStyle(obj, 'fontWeight') === 'underline';
    setStyle(obj, 'underline', isUnderlined ? 'normal' : 'underline');
    obj.dirty = true;
});

addHandler('text-color', function (obj) {
    setStyle(obj, 'fill', this.value);
}, 'onchange');

addHandler('text-background', function (obj) {
    setStyle(obj, 'textBackgroundColor', this.value);
}, 'onchange');

addHandler('text-font-family', function (obj) {
    setStyle(obj, 'fontFamily', this.value);
}, 'onchange');

addHandler('text-font-size', function (obj) {
    setStyle(obj, 'fontSize', this.value);
}, 'onchange');

function updateTextAlignIcon(alignment) {
    let src = 'assets/tool-text-align-' + alignment + '.svg';
    alignment[0] = alignment[0].toUpperCase();
    $('#text-align-icon').attr('src', src);
    $('#text-align-icon').attr('alt', alignment);
}

addHandler('text-align-left', function (obj) {
    obj.set('textAlign', 'left');
});

addHandler('text-align-center', function (obj) {
    obj.set('textAlign', 'center');
});

addHandler('text-align-right', function (obj) {
    obj.set('textAlign', 'right');
});

// Add text to the canvas
function addText() {
    const defaultTextString = 'Write here';

    var text = new fabric.IText(defaultTextString, {
        left: 100,
        top: 100,
        width: 100,
        fontFamily: 'sans-serif',
        fontSize: 18,
        textAlign: 'left',
    });

    text.setControlsVisibility({
        mt: false, // middle top 
        mb: false, // midle bottom
        ml: false, // middle left
        mr: false, // middle right
        tl: false, //top left
        tr: false, //top right
        bl: false, //bottom left
        br: false //bottom right
    });

    // When the text changes, we'll verify whether it's empty or has its default value. 
    // If that's the case, we'll delete it
    text.on('editing:exited', function () {
        if (text.text.trim() === '' || (text.text === defaultTextString && text.isFirstEdit)) {
            canvas.remove(text);
            canvas.renderAll();
            // Prevent saving the diagram state if the text was just created and unmodified (created by error)
            if (!text.isFirstEdit) saveDiagramStateToUndoStack();
        }
        if (text && text.isFirstEdit) {
            text.isFirstEdit = false;
        }
    });

    canvas.add(text).setActiveObject(text);
    text.selectAll().enterEditing();
    text.isFirstEdit = true;
}

// Handle text-related events to show and update text tool's panel
function textSelectionChange(e) {
    // Update color pickers
    var textColor_colorPicker = document.getElementById('text-color');
    var textBackground_colorPicker = document.getElementById('text-background');
    textColor_colorPicker.value = getStyle(obj, 'fill');
    textBackground_colorPicker.value = getStyle(obj, 'textBackgroundColor');

    // Update text alignment icon
    updateTextAlignIcon(obj.get('textAlign'));

    // Update font family and size
    $('#text-font-family').val(getStyle(obj, 'fontFamily') || obj.get('fontFamily'));
    $('#text-font-size').val(getStyle(obj, 'fontSize') || obj.get('fontSize'));
}

function enterTextEditor(e) {
    console.log('Editing text');
    $('#panel-text').removeClass('hide');
    textSelectionChange(e);
}
