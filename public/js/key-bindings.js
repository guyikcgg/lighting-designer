function isTextEditing() {
    // Check if the active element is an input or textarea
    const activeElement = document.activeElement;
    if (activeElement.tagName === 'INPUT' || activeElement.tagName === 'TEXTAREA') {
        return true;
    }
    
    // Check if Fabric.js is in text editing mode
    if (canvas.getActiveObject() && canvas.getActiveObject().isEditing) {
        return true;
    }
    
    return false;
}

document.addEventListener('keydown', function(event) {
    // Detect Ctrl+Z for undo
    if (event.ctrlKey && event.key === 'z' && !event.shiftKey) {
        event.preventDefault();
        $('#undo').prop('disabled') || $('#undo').click();
    }
    // Detect Ctrl+Shift+Z for redo
    else if (event.ctrlKey && event.key === 'Z' && event.shiftKey) {
        event.preventDefault();
        $('#redo').prop('disabled') || $('#redo').click();
    }
    // Detect Delete key
    else if (event.key === 'Delete' && !isTextEditing()) {
        event.preventDefault();
        deleteActiveObject();
    }
    // Detect Page Down for send backwards
    else if (event.key === 'PageDown' && !isTextEditing()) {
        event.preventDefault();
        sendActiveObjectBackwards();
    }
    // Detect Page Up for bring forward
    else if (event.key === 'PageUp' && !isTextEditing()) {
        event.preventDefault();
        bringActiveObjectForward();
    }
    // Detect H for flip horizontally (x axis)
    else if (!event.ctrlKey && event.key === 'h' && !isTextEditing()) {
        event.preventDefault();
        flipActiveObject('x');
    }
    // Detect V for flip vertically (y axis)
    else if (!event.ctrlKey && event.key === 'v' && !isTextEditing()) {
        event.preventDefault();
        flipActiveObject('y');
    }
    // Detect Ctrl+D for duplicate/clone
    else if (event.ctrlKey && event.key === 'd' && !isTextEditing()) {
        event.preventDefault();
        duplicateActiveObject();
    }
    // Detect Ctrl+S for save
    else if (event.ctrlKey && event.key === 's') {
        event.preventDefault();
        saveJSON();
    }
    // Detect Escape key
    else if (event.key === 'Escape') {
        event.preventDefault();
        canvas.discardActiveObject().renderAll();
    }
    // Detect Shift+N for new diagram
    // Key is 'N' because shift modifies lowercase 'n' to 'N'
    // It is changed to capital N because when we Shift it is capital N.
    else if (event.shiftKey && !event.ctrlKey && event.key === 'N') {
        event.preventDefault();
        newDiagram();
    }
    // Detect Ctrl+I for import image
    else if (event.ctrlKey && event.key === 'i') {
        event.preventDefault();
        addCustomImage();
    }
    // Detect T for add text
    else if (!event.ctrlKey && event.key === 't' && !isTextEditing()) {
        event.preventDefault();
        addText();
    }
});
