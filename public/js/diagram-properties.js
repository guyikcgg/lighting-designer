
function refreshDocumentTitle() {
    document.title = canvas.diagramProperties.name + (isDiagramSaved ? "" : "*") + " - Lighting Designer";
}

function setDiagramBackgroundColor(color) {
    color ||= "#ffffff";

    canvas.backgroundColor = color;
    $('#diagram-background').val(color);
    canvas.renderAll();
}

function renderDiagramPanel() {
    $('#panel-diagram').removeClass('hide');
    $('#diagram-name').val(canvas.diagramProperties.name);
    $('#diagram-notes').val(canvas.diagramProperties.notes);
    setDiagramBackgroundColor(canvas.backgroundColor);
}

// Add custon properties to the canvas
function addDiagramProperties(e) {
    canvas.diagramProperties ??= {};
    fabric.Canvas.prototype.toJSON = (function (toJSON) {
        return function (propertiesToInclude) {
            return fabric.util.object.extend(
                toJSON.call(this, propertiesToInclude),
                { diagramProperties: this.diagramProperties }
            );
        };
    })(fabric.Canvas.prototype.toJSON);
}

$('#diagram-background').change(function() {
    if (canvas.backgroundColor != this.value) {
        setDiagramBackgroundColor(this.value);
        saveDiagramStateToUndoStack();
    }
});

$('#diagram-name').focusout(function(e) {
    if (canvas.diagramProperties.name != this.value) {
        canvas.diagramProperties.name = this.value;
        saveDiagramStateToUndoStack();
    }
});

$('#diagram-notes').focusout(function(e) {
    if (canvas.diagramProperties.notes != this.value) {
        canvas.diagramProperties.notes = this.value;
        saveDiagramStateToUndoStack();
    }
});
