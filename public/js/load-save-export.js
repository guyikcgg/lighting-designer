
/**
 * Functions to save and load app data. Functions to export to different file formats.
 */

let isDiagramSaved = true;

function markDiagramAsSaved() {
    isDiagramSaved = true;
    refreshDocumentTitle();
}

function markDiagramAsUnsaved() {
    isDiagramSaved = false;
    refreshDocumentTitle();
}

// Load diagram from a JSON string
function loadFromJSON(json, func) {
    canvas.diagramProperties = { name: 'My diagram' };

    canvas.loadFromJSON(json, function () {
        canvas.renderAll.bind(canvas);

        // Assing assets their corresponding class
        canvas.getObjects().forEach(obj => {
            addAssetClassProperties(obj);
        });

        // Start with fresh undo/redo stacks
        clearUndoRedoStacks();
        markDiagramAsSaved();

        if (typeof func === 'function') {
            func();
        }
    });
}

//
// UI / Top level functions
//

// Let the user choose a JSON file and load diagram from that file
function loadJSON() {
    const input = document.createElement('input');
    input.type = 'file';
    input.accept = 'application/JSON';

    input.onchange = function (e) {
        // getting a hold of the file reference
        var file = e.target.files[0];

        // setting up the reader
        var reader = new FileReader();
        reader.readAsText(file); // this is reading as text

        // here we tell the reader what to do when it's done reading...
        reader.onload = function (readerEvent) {
            loadFromJSON(readerEvent.target.result, function () {
                // This is in case of not having an actual diagram name
                canvas.diagramProperties.name ??= file.name.replace(/\.[^/.]+$/, "");
                refreshDocumentTitle();

                // Clear the selection to start fresh
                canvas.discardActiveObject();
                canvas.renderAll();
            });
        }
    }

    if (isDiagramSaved || confirm('There are unsaved changes. Loading a diagram will discard these changes. Proceed?')) {
        input.click();
    }
}

// Save diagram to a JSON file (currently, just the canvas with FabricJS)
function saveJSON() {
    if (!isDiagramSaved) {
        canvas.diagramProperties.selectedObjectIndex = getSelectedObjectIndex();
    
        let json = canvas.toJSON();
        let file = new Blob([JSON.stringify(json)], { type: 'text/plain' });
        let downloadLink = document.createElement('a');
        downloadLink.href = URL.createObjectURL(file);
        downloadLink.setAttribute('download', canvas.diagramProperties.name + '.json');
        downloadLink.click();
        markDiagramAsSaved();
    } else {
        console.log("Diagram already saved!")
    }
}

//
// Export functions
//

function downloadSVG() {
    canvas.discardActiveObject().renderAll();

    let svg = canvas.toSVG();
    let downloadLink = document.createElement('a');
    downloadLink.setAttribute('href', 'data:image/svg+xml;base64,' + btoa(svg));
    downloadLink.setAttribute('download', canvas.diagramProperties.name + '.svg');
    downloadLink.click();
}

function downloadPNG() {
    canvas.discardActiveObject().renderAll();

    let canvasHTML = document.getElementById('c');
    canvasHTML.toBlob(function (blob) {
        let url = URL.createObjectURL(blob);
        let downloadLink = document.createElement('a');
        downloadLink.setAttribute('href', url);
        downloadLink.setAttribute('download', canvas.diagramProperties.name + '.png');
        downloadLink.click();
    });

}

/* Use jsPDF to generate a PDF from the HTML canvas */
const { jsPDF } = window.jspdf;
function downloadPDF() {
    // We may want to check out https://stackoverflow.com/questions/16858954/how-to-properly-use-jspdf-library
    // Also check out http://raw.githack.com/MrRio/jsPDF/master/docs/module-svg.html
    const marginLR = 25;

    canvas.discardActiveObject().renderAll();

    let canvasHTML = document.getElementById('c');
    let imgData = canvasHTML.toDataURL("image/png");
    let pdf = new jsPDF({
        orientation: 'p',
        unit: 'mm',
        format: 'a4',
        putOnlyUsedFonts: true,
        floatPrecision: 16 // or "smart", default is 16
    });

    // Add diagram title
    pdf.setFont("helvetica", "normal", "bold");
    pdf.setFontSize(18);
    pdf.text(canvas.diagramProperties.name, marginLR, 25);

    // Add canvas as an image
    let aspectRatio = canvasHTML.width / canvasHTML.height;
    let imgW = 210 - marginLR * 2;
    let imgH = imgW / aspectRatio;
    pdf.addImage(imgData, 'PNG', marginLR, 40, imgW, imgH);

    // Add diagram notes
    pdf.setFont("helvetica", "normal", "normal");
    pdf.setFontSize(10);
    let textLines = pdf.splitTextToSize(canvas.diagramProperties.notes, imgW);
    pdf.text(textLines, marginLR, 40 + imgH + 15);

    // Add credits (URL should render as a link)
    pdf.setFont("helvetica", "normal", "normal");
    pdf.setFontSize(10);
    pdf.text('Diagram generated with Lighting Designer (https://lighting-designer.app/)', marginLR, 297 - 25);

    // Download PDF
    pdf.save(canvas.diagramProperties.name + ".pdf");
}

//
// Commands (non-functions)
//

// Ask for confirmation to exit
window.addEventListener('beforeunload', function (e) {
    if (!isDiagramSaved) {
        e.preventDefault();
        e.returnValue = true;
    }
});
