function shape(type, width = 100, height = 100, left = 50, top = 50) {
    var shapeType;

    shapeOptions = {
        left: left,
        top: top,
        fill: '#d3d3d3',
        objectCaching: false,
        stroke: '#a9a9a9',
        strokeWidth: 2,
        strokeUniform: true,
    };

    switch (type) {
        case "rect":
            shapeOptions.width = width;
            shapeOptions.height = height;
            var obj = new fabric.Rect(shapeOptions);
            shapeType = "Rectangle";
            break;

        case "circle":
            shapeOptions.radius = Math.min(width, height) / 2;
            var obj = new fabric.Circle(shapeOptions);
            shapeType = "Circle";
            break;

        case "triangle":
            shapeOptions.width = width;
            shapeOptions.height = Math.sqrt(3) * width / 2;
            var obj = new fabric.Triangle(shapeOptions);
            shapeType = "Triangle";
            break;

        default:
            console.error("unknown shape type");
            break;
    }

    obj.setControlsVisibility({
        mt: true, // middle top 
        mb: true, // midle bottom
        ml: true, // middle left
        mr: true, // middle right
        tl: true, //top left
        tr: true, //top right
        bl: true, //bottom left
        br: true //bottom right
    });

    canvas.add(obj);
    obj.objProperties.name = shapeType;
    canvas.setActiveObject(obj);
    saveDiagramStateToUndoStack();
}

$('#shape-fill').change(function() {
    canvas.getActiveObject().set('fill', this.value);
    canvas.renderAll();
    saveDiagramStateToUndoStack();
});

$('#shape-stroke').change(function() {
    canvas.getActiveObject().set('stroke', this.value);
    canvas.renderAll();
    saveDiagramStateToUndoStack();
});

$('#shape-stroke-width').change(function() {
    canvas.getActiveObject().set('strokeWidth', Number(this.value));
    canvas.renderAll();
    saveDiagramStateToUndoStack();
});

$('#shape-name').focus(function() {
    this.activeObject = canvas.getActiveObject();
    this.select();
});

$('#shape-name').focusout(function(e) {
    // FIXME If a new object is selected, the id of the newly selected object will be taken,
    // because this.value is updated before the events trigger (see app.js)
    // Also, the id should be saved when pressing enter or Ctrl+S
    this.activeObject.objProperties.name = this.value;
    saveDiagramStateToUndoStack();
});
