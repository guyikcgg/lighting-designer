function sendActiveObjectBackwards() {
    var activeObject = canvas.getActiveObject();
    if (activeObject) {
        canvas.sendBackwards(activeObject, true);
        canvas.renderAll();
        saveDiagramStateToUndoStack();
    }
}

function bringActiveObjectForward() {
    var activeObject = canvas.getActiveObject();
    if (activeObject) {
        canvas.bringForward(activeObject, true);
        canvas.renderAll();
        saveDiagramStateToUndoStack();
    }
}

function deleteActiveObject() {
    var activeObjects = canvas.getActiveObjects();

    if (activeObjects.length > 0) {
        activeObjects.forEach(function(e) {
            canvas.remove(e);
        });
        canvas.discardActiveObject();
        canvas.renderAll();
        saveDiagramStateToUndoStack();
    }
}

function duplicateActiveObject() {
    var activeObject = canvas.getActiveObject();

    if (activeObject) {
        // Make sure this is a single object
        if (activeObject.type == 'activeSelection') {
            alert('Duplicating multiple objects is not yet supported!');
            return;
        }

        activeObject.clone(function (cloned) {
            cloned.left += 10;
            cloned.top += 10;
            addAssetClassProperties(cloned);
            canvas.add(cloned);
            canvas.setActiveObject(cloned);
        });
        canvas.renderAll();
        saveDiagramStateToUndoStack();
    }
}

function flipActiveObject(axis) {
    var activeObject = canvas.getActiveObject();
    if (activeObject) {
        switch (axis) {
            case 'x':
            case 'X':
                activeObject.set('flipX', !activeObject.get('flipX'));
                break;
            case 'y':
            case 'Y':
                activeObject.set('flipY', !activeObject.get('flipY'));
                break;
        }
        canvas.renderAll();
        saveDiagramStateToUndoStack();
    }
}

function toggleShadowActiveObject() {
    var activeObject = canvas.getActiveObject();
    if (activeObject) {
        if (activeObject.get('shadow')) {
            activeObject.set('shadow', null);
        } else {
            activeObject.set(
                {
                    shadow: {
                        color: 'rgba(0, 0, 0, 0.35)',
                        blur: 10,
                        offsetX: 2,
                        offsetY: 2
                    }
                }
            );
        }
        canvas.renderAll();
        saveDiagramStateToUndoStack();
    }
}
