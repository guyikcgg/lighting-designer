/* Original script: https://stackoverflow.com/a/32618231/3761766 */

// current unsaved state
var diagramState;
// past states
var undoStack = [];
// reverted states
var redoStack = [];
// don't save while undoing or redoing
var isRestoringDiagram = false;

/**
 * Get the index of the selected object (if any)
 * @returns index of the selected object
 */
function getSelectedObjectIndex() {
    var activeObject = canvas.getActiveObject();
    var selectedObjectIndex = null;

    if (activeObject) {
        var objects = canvas.getObjects();
        selectedObjectIndex = objects.indexOf(activeObject);
    }

    return selectedObjectIndex;
}

/**
 * Select canvas object based on its index
 * @param objectIndex index of the object to be selected
 */
function selectObjectByIndex(objectIndex) {
    canvas.discardActiveObject();

    if (objectIndex !== null) {
        var objects = canvas.getObjects();
        if (objectIndex < objects.length) {
            var objectToSelect = objects[objectIndex];
            canvas.setActiveObject(objectToSelect);
        }
    }
    canvas.renderAll();
}

/**
* Push the current state into the undo stack and then capture the current state
*/
function saveDiagramStateToUndoStack() {
    markDiagramAsUnsaved();

    if (!isRestoringDiagram) {
        // clear the redo stack
        redoStack = [];
        $('#redo').prop('disabled', true);
        $('#undo').prop('disabled', true);
        // initial call won't have a state
        if (diagramState) {
            // Save the state into the stack
            undoStack.push(diagramState);
            $('#undo').prop('disabled', false);
        }
        
        canvas.diagramProperties.selectedObjectIndex = getSelectedObjectIndex();
        // Clone the state so that it's an independent object and it's not affected by changes to the canvas
        diagramState = structuredClone(canvas.toJSON());
    }
}

/**
* Save the current state in the redo stack, reset to a state in the undo stack, and enable the buttons accordingly.
* Or, do the opposite (redo vs. undo)
* @param playStack which stack to get the last state from and to then render the canvas as
* @param saveStack which stack to push current state into
* @param buttonsOn jQuery selector. Enable these buttons.
* @param buttonsOff jQuery selector. Disable these buttons.
*/
function restoreDiagramFromStack(playStack, saveStack, buttonsOn, buttonsOff, doneCallback) {
    isRestoringDiagram = true;
    saveStack.push(diagramState);
    diagramState = playStack.pop();
    var on = $(buttonsOn);
    var off = $(buttonsOff);
    // turn both buttons off for the moment to prevent rapid clicking
    on.prop('disabled', true);
    off.prop('disabled', true);
    canvas.clear();
    canvas.loadFromJSON(diagramState, function () {
        canvas.renderAll.bind(canvas);

        // Assing assets their corresponding class
        canvas.getObjects().forEach(obj => {
            addAssetClassProperties(obj);
        });

        // now turn the buttons back on if applicable
        on.prop('disabled', false);
        if (playStack.length) {
            off.prop('disabled', false);
        }

        renderDiagramPanel();

        markDiagramAsUnsaved();
        isRestoringDiagram = false;

        doneCallback();
    });
}

function clearUndoRedoStacks() {
    $('#redo').prop('disabled', true);
    $('#undo').prop('disabled', true);
    undoStack = [];
    redoStack = [];
    // diagramState is independent from the canvas (not linked to it)
    diagramState = structuredClone(canvas.toJSON());
}

// undo and redo buttons
$('#undo').click(function () {
    let selectedObjectIndex = canvas.diagramProperties.selectedObjectIndex;

    restoreDiagramFromStack(undoStack, redoStack, '#redo', this, function() {
        selectObjectByIndex(selectedObjectIndex)
    });
});

$('#redo').click(function () {
    restoreDiagramFromStack(redoStack, undoStack, '#undo', this, function() {
        selectObjectByIndex(canvas.diagramProperties.selectedObjectIndex)
    });
})
