$(document).foundation()

// Set up the canvas
var canvas = this.__canvas = new fabric.Canvas('c', { preserveObjectStacking: true });
resizeCanvas();
preventObjectsEscapingTheCanvas();
setupCustomControls();

// Customize controls
//fabric.Object.prototype.transparentCorners = false;
//fabric.Object.prototype.cornerColor = 'blue';
fabric.Object.prototype.cornerStyle = 'circle';

// Dark mode toggle
function darkMode() {
    var body = document.body;
    body.classList.toggle('dark-mode');
}

// Show the panel corresponding to the selected object
// Source: https://stackoverflow.com/questions/58407403/event-handler-for-object-selection-on-fabricjs-canvas
canvas.on({
    'selection:updated': handleSelectedElements,
    'selection:created': handleSelectedElements,
    'text:editing:entered': enterTextEditor,
    'text:selection:changed': textSelectionChange,
    'selection:cleared': handleClearedSelection,
    'object:added': addObjProperties,
    'object:modified': saveDiagramStateToUndoStack,
});

function handleClearedSelection() {
    hidePanels();
    renderDiagramPanel();
}

function handleSelectedElements(e) {
    hidePanels();

    $('#panel-object').removeClass('hide');

    let n = e.selected.length;
    if (n > 1) {
        console.log(n + ' objects selected');
    } else {
        obj = e.selected[0];

        //Handle the object here
        switch (obj.type) {
            case 'i-text':
                console.log('Text object selected');
                // Text tool's panel shows on text:editing:entered
                break;
            case 'group':
                let asset = obj.assetClassProperties;
                let objProperties = obj.objProperties;
                if (asset) {
                    console.log('1 asset selected: obj.assetClassProperties.id=' + obj.assetClassProperties.id);
                    asset.panel.removeClass('hide');
                    asset.nameInput.val(objProperties.name);

                    // Handle properties (get buttons' status from the selected asset)
                    if (asset.properties) {
                        asset.properties.forEach(function (p) {
                            switch (p.type) {
                                case "color":
                                    p.control.val(obj.getObjects()[p.n].get('fill'));
                                    break;
                                case "select":
                                    p.control.val(obj.objProperties.model);
                                    break;
                            }
                        });
                    }
                } else {
                    console.log('1 asset selected: obj.assetClassProperties=' + obj.assetClassProperties);
                }
                break;
            case 'rect':
            case 'circle':
            case 'triangle':
                $('#panel-shape').removeClass('hide');
                $('#shape-fill').val(obj.fill);
                $('#shape-stroke').val(obj.stroke);
                $('#shape-stroke-width').val(obj.strokeWidth);
                $('#shape-name').val(obj.objProperties.name);
                break;
            default:
                console.log('1 object selected: obj.type=' + obj.type);
                break;
        }
    }

}

// Create a new diagram
function newDiagram() {
    // Ask for confirmation to clear the canvas
    if (isDiagramSaved || confirm('Are you sure you want to clear the diagram? This action cannot be undone!')) {
        canvas.clear();
        canvas.diagramProperties = { name: 'My diagram' };
        renderDiagramPanel();

        clearUndoRedoStacks();
        markDiagramAsSaved();
    }
}

function hidePanels(e) {
    $('.right-panel').addClass('hide');
}

addDiagramProperties();

// Add a callback to the asset so that objProperties are exported to JSON
function addObjProperties(e) {
    e.target.objProperties ??= {};

    e.target.toObject = (function (toObject) {
        return function (propertiesToInclude) {
            return fabric.util.object.extend(
                toObject.call(this, propertiesToInclude),
                { objProperties: this.objProperties }
            );
        };
    })(e.target.toObject);
}

// Add default objects to canvas (when the document has fully loaded)
window.addEventListener("load", function () {
    fetch('assets/default-diagram.json').then(
        function (u) { return u.json(); }
    ).then(
        function (json) { loadFromJSON(json, handleClearedSelection); }
    );
});
