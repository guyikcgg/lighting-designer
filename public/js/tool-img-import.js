// Add custom images -- original source: https://stackoverflow.com/questions/21715812/how-to-upload-an-image-to-a-canvas-with-fabric-js
document.getElementById('tool-img-import-loader').onchange = function handleImage(e) {
    var reader = new FileReader();

    reader.onload = function (event) {
        var imgObj = new Image();
        imgObj.src = event.target.result;

        imgObj.onload = function () {
            var image = new fabric.Image(imgObj);

            max_width = canvas.width - 2 * 100;
            max_height = canvas.height - 2 * 100;

            if (image.width * image.scaleX > max_width) {
                image.scaleToWidth(max_width);
            }
            if (image.height * image.scaleY > max_height) {
                image.scaleToHeight(max_height);
            }

            canvas.add(image);
            image.set({
                left: 100,
                top: 100,
            });

            canvas.setActiveObject(image);
            canvas.renderAll();
            
            saveDiagramStateToUndoStack();
        }
    }

    reader.readAsDataURL(e.target.files[0]);
}

function addCustomImage() {
    var fileSelector = document.getElementById('tool-img-import-loader');

    fileSelector.click();
    fileSelector.value = "";
}
