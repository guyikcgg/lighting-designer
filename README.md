
# Lighting Designer

A libre web app to design lighting setups for cinema and photography.

Lighting Designer aims to help photographers and cinematographers to design lighing setups for professional photo shoots and film recording. You can run Lighting Designer from your browser at https://lighting-designer.app/.


## Features

- Desing your lighting setup in top view
- Common light sources and accessories available out-of-the-box
- Customizable light filters
- Enrich your diagrams with a variety of subjects, objects and shapes
- Add beautiful notes with styled text
- Customize objects' size and color
- Insert and manipulate custom objects from offline images
- Save your setups in JSON to work on them later
- Export your setups in SVG (editable), PNG and PDF


### Technical features

- Undo/redo without limits
- Cross platform (web based, only desktop)
- Fully offline, your data remains in your computer
- Light/dark mode toggle
- Free software, you are free to use, study, share and improve this software (see `License.md`)

![ilovefs.org](https://fsfe.org/contribute/promopics/ilovefs-sticker_thumb.png)

## Coming soon

Lighting Designer improves day by day. Here are some features on the roadmap:

- Create a rider with every element on the setup
- Insert text notes based on objects' parameters
- Visualize light coverage and camera field of view
- Freely draw arrows or other notes


### UI/UX

- Group up and lock elements
- Implement a right-click menu
- Redesign UI to mimic common desktop apps
- Smartphone-ready


### Desirable features for the future

- Visualize lighting in 3D and allow to experiment with lighting parameters
- Lighting parameter calculations, including exposure, color temperature, etc.


## Contributing

Contributions are always welcome!

You can report bugs or request improvements on [GitLab issues](https://gitlab.com/guyikcgg/lighting-designer/-/issues/).

If you like drawing vector graphics, you can also contribute missing lighting sources, accessories, or any other asset.

If you like coding, take a look at current [GitLab issues](https://gitlab.com/guyikcgg/lighting-designer/-/issues/) or feel free to refactor or to implement features that you find interesting. Also, I'm planning to port the whole project to *Vue.js*. Help is very welcome!

**Important note**

> Since I started this project for fun, it follows a spaghetti code design pattern and scattergun approach to develpment. Please don't be scared. That said, merge requests are thoroughtly reviewed and some structure is expected.

Please contact me directly if you plan to contribute.

- https://mastodon.social/@guyikcgg
- XMPP: guyikcgg@peeko.chat
- email: guyik.cgg@gmail.com


## License

[GNU AGPLv3](https://choosealicense.com/licenses/agpl-3.0/)


## Acknowledgements

Good tools are the product of impetuous needs. In this case, I want to acknowledge Celestino Jorge López Catalán, Lorena Villanueva García, and their demading hands-on assignments in which we had to design dozens of lighting setups.

That said, this software stands on the shoulders of giants. Without them, this application wouldn't exist.


### Inspiration

- [Online Lighting Diagram Creator](http://lightingdiagrams.com/Creator)
- Keving Kertz's great `LightingSetup.psd` file


### Software sources and tools

- [Fabric.js](http://fabricjs.com/)
- [Foundation Framewor](https://get.foundation/)
- [jsPDF](https://github.com/parallax/jsPDF)
- [jQuery](https://jquery.com/)
- The awesome community at [StackOverflow](https://stackoverflow.com/)
- Original graphics designed in [Inkscape](https://inkscape.org/)
- Some graphics taken from [Openclipart](https://openclipart.org/)
